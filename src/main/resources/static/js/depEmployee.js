$(document).ready(function() {

});

var employeeCache = '';


function createTable(val){
    var table =  val;
    var result="<thead><tr><td><div class='name_table main_cell'>Ученики</div></td>"+
    "<td>Имя</td><td>Фамилия</td><td>Отчество</td><td>Телефон</td><td>Почта</td><td>Действия</td></tr></thead><tbody>";
    for (i=0; i<table.length;i++) {
        var id = table[i].id;
        var name = table[i].name;
        var surname = table[i].surname;
        var patronymic = table[i].patronymic;
        var position = table[i].position;
        var mail = table[i].contacts.mail;
        var phone = table[i].contacts.phone;
        var departmentId = table[i].department.id
        var departmentName = table[i].department.name;
        result+="<tr id='empTr"+i+"'>" +
            "<td></td>"+
            "<td>"+ name + "</td>"+
            "<td>"+ surname + "</td>"+
            "<td>"+ patronymic + "</td>"+
            "<td>"+ phone + "</td>"+
            "<td>"+ mail + "</td>"+
            "<td><div class='name_table'><button class='update_emp update_icon' onClick='update_emp("+id+", "+departmentId+", \""+departmentName+"\","+i+", \""+name+"\", \""+surname+"\",\""+phone+"\", \""+mail+"\", \""+patronymic+"\"), clearDepartment()'></button></div></td>"+
            "</tr>";
    }
    result+= "</tbody>"
    $('#employeeDepartment').html(result);
    employeeCache = result;
    paginationE();
}

function clearDepartment(){
$("#department_wrapper").css({ "display": "none"});
}

function paginationE(){
    $('#employeeDepartment').DataTable( {
            "bJQueryUI":true,
            "bSort":false,
            "bPaginate":true, // Pagination True
            "sPaginationType":"simple_numbers", // And its type.
            "iDisplayLength": 7,
            "searching": true,
            "info": false,
            "bLengthChange": false,
            "destroy": true,
            "language": {
                "search": "Поиск ",
                "paginate": {
                      "previous": "назад",
                      "next": "вперед"
                    },
                    "emptyTable": "Таблица пуста"
              }
        } );
}

function update_emp(id, departmentId, departmentName, idTr, name, surname, phone, mail, patronymic){
    var result="<tr>"+
        "<td></td>"+
        "<td><input id='updating_name' type='text' value='"+name+"'></td>"+
        "<td><input id='updating_surname' type='text' value='"+surname+"'></td>"+
        "<td><input id='updating_patronymic' type='text' value='"+patronymic+"'></td>"+
        "<td><input id='updating_phone' type='text' value='"+phone+"'></td>"+
        "<td><input id='updating_mail' type='text' value='"+mail+"'></td>"+
        "<td><div class='name_table'><button class='accept_update yes_icon' onClick='accept_update("+id+", \""+departmentName+"\", "+departmentId+")'></button></div>"+
        "<div class='name_table'><button class='cancel_update no_icon' onClick='cancel_update(\""+departmentName+"\", "+departmentId+")'></button></div></td>"+
        "</tr>"

        $("#empTr"+idTr).replaceWith(result);
}

function accept_update(id, departmentName, departmentId){
    var nameEmp =  $('#updating_name').val();
    var surnameEmp =  $('#updating_surname').val();
    var patronymicEmp =  $('#updating_patronymic').val();
    var phoneEmp =  $('#updating_phone').val();
    var mailEmp =  $('#updating_mail').val();
    getAllDepartment();
    $.ajax({
            url: '/employee',
            contentType: "application/json",
            type: 'put',
            data: JSON.stringify({employee:{name:nameEmp,
                                                surname:surnameEmp,
                                                patronymic: patronymicEmp,
                                                contacts:{phone:phoneEmp,
                                                          mail:mailEmp
                                                        },
                                                department:{id:departmentId,
                                                            name:departmentName
                                                        }
                                                }, id}),

            complete : function() {
                get_all_employee_by_dep(departmentId, departmentName);
                $("#department_wrapper").css({ "display": "block"});
            },
            success : function(data, textStatus, jqXHR){
                console.log("dep_update_done")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("dep_update_error")


            }
        });
}

function cancel_update(departmentName, departmentId){
    get_all_employee_by_dep(departmentId, departmentName);
    $("#department_wrapper").css({ "display": "block"});
}


function get_all_employee_by_dep(depId, depName){
    console.log("get_all_employee_by_dep click")
    $.ajax({
            url : '/employee/dep'+depId,
            type: 'get',
            success : function(data) {
                console.log(data);
                createTable(data, depId, depName);
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
}