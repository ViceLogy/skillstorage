$(document).ready(function() {

});

employeeCacheE = '';

function getAllEmployee(){
    $("#mainInfo").css({'display':'none'});
    $.ajax({
        url : '/employee/all',
        type: 'get',
        success : function(data) {
            console.log(data);
            createTableE(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });
}

function createTableE(val){
    var table = val;
    var searchDiv = "<div id='searchDiv'><div class='SkillSearch'>Поиск умений</div><div class='name_table'><input id='searchSkill' class='inputSearch' type='text'><div/>"+
    "<div class='name_table'><button class='search_icon' onClick='searchEmployeeBySkill(), clearSkill()'></button></div></div>";

    result="<thead><tr><td><div class='name_table main_cell'>Ученики</div><button class='add_icon name_table' onclick='add_emp(), getAllDepartmentFromAddedEmployee()'></button></td>"+
    "<td>Имя</td><td>Фамилия</td><td>Отчество</td><td>Телефон</td><td>Почта</td><td>Класс</td><td>Действия</td></tr></thead><tbody>"
    console.log("createTableE")
    for (i=0; i<table.length;i++) {
            var id = table[i].id;
            var name = table[i].name;
            var surname = table[i].surname;
            var patronymic = table[i].patronymic;
            var mail = table[i].contacts.mail;
            var phone = table[i].contacts.phone;
            var departmentId = table[i].department.id;
            var departmentName = table[i].department.name;
            result+="<tr id='empTrE"+i+"'>" +
                "<td id='sizeBlock'></td>"+
                "<td id='sizeBlock' onClick='getSkillsByIdEmp("+id+"), getSkillsByIdEmp("+id+")'>"+ name +"</td>"+
                "<td id='sizeBlock' onClick='getSkillsByIdEmp("+id+"), getSkillsByIdEmp("+id+")'>"+ surname +"</td>"+
                "<td id='sizeBlock' onClick='getSkillsByIdEmp("+id+"), getSkillsByIdEmp("+id+")'>"+ patronymic +"</td>"+
                "<td id='sizeBlock' onClick='getSkillsByIdEmp("+id+"), getSkillsByIdEmp("+id+")'>"+ phone + "</td>"+
                "<td id='sizeBlock' onClick='getSkillsByIdEmp("+id+"), getSkillsByIdEmp("+id+")'>"+ mail + "</td>"+
                "<td id='sizeBlock' onClick='getSkillsByIdEmp("+id+"), getSkillsByIdEmp("+id+")'>"+ departmentName + "</td>"+
                "<td id='sizeBlock'><div class='name_table'><button class='delete_empE del_icon' onClick='delete_empE("+id+")'></button></div>"+
                "<div class='name_table'><button class='update_icon' onClick='updateEmployee("+id+",\""+name+"\", \""+surname+"\", \""+patronymic+"\", \""+phone+"\", \""+mail+"\", "+i+"), getAllDepartmentFromUpdateEmployee("+departmentId+", \""+departmentName+"\")'></button></div></td>"+
                "</tr>";
        }
        result+= "</tbody>"
        $('#employee').html(result);
        $('#searchMainDiv').html(searchDiv);
        employeeCacheE = result;
        paginationEmp();
        $(".dataTables_filter").children("label").children("input").css({ "-webkit-appearance": "button !IMPORTANT"});
    }

function add_emp(){
    console.log("add_emp click!")
    var result= employeeCacheE;

    result+="<tr>"+
            "<td></td>"+
            "<td><input id='name_emp' type='text' name='name'></td>"+
            "<td><input id='surname_emp' type='text' name='name'></td>"+
            "<td><input id='patronymic_emp' type='text' name='name'></td>"+
            "<td><input id='phone_emp' type='text' name='name'></td>"+
            "<td><input id='mail_emp' type='text' name='name'></td>"+
            "<td id='allDepartment'></td>"+
            "<td><div class='name_table'><button class='accept_save yes_icon' onClick='accept_save()'></button></div>"+
            "<div class='name_table'><button class='cancel_save no_icon' onClick='cancel_save()'></button></div></td>"+
            "</tr>";

            $('#employee').html(result);
}

function createSelectorFromAdded(values){
    var tables = values;
    var result = "<select class='depList'>";
    for (i=0; i<tables.length;i++) {
            var departmentId = tables[i].id;
            var departmentName = tables[i].name;
    result+="<option id="+departmentId+" value=\""+departmentName+"\">"+departmentName+"</option>";

    }
    result+="</select>";
    console.log(result);
    $('#allDepartment').html(result);
}

function accept_save(depId, depName){
    var nameEmp = $('#name_emp').val();
    var surnameEmp = $('#surname_emp').val();
    var patronymicEmp = $('#patronymic_emp').val();
    var phoneEmp = $('#phone_emp').val();
    var mailEmp = $('#mail_emp').val();
    var departmentId = $(".depList").children("option:selected").attr('id');
    var departmentName = $(".depList").children("option:selected").val();
        $.ajax({
             url : '/employee',
             contentType: "application/json",
             type : 'post',
             data: JSON.stringify({name:nameEmp,
                surname:surnameEmp,
                patronymic:patronymicEmp,
                contacts:{
                phone:phoneEmp,
                mail:mailEmp
                },
                department:{
                id: departmentId,
                name:departmentName
                }

             }),
             complete : function() {
                 console.log("emp_save_done");
                 getAllEmployee();
             },
             success : function(data, textStatus, jqXHR){
             },
             error: function (jqXHR, textStatus, errorThrown) {
                alert("Такой ученик уже существует!")
            }
        });
}

function cancel_save(){
    getAllEmployee();
}

function updateEmployee(id ,name, surname, patronymic, phone, mail, trId){
        var result="<tr>"+
            "<td></td>"+
            "<td><input id='updating_name_employee' type='text' value='"+name+"'></td>"+
            "<td><input id='updating_surname_employee' type='text' value='"+surname+"'></td>"+
            "<td><input id='updating_patronymic_employee' type='text' value='"+patronymic+"'></td>"+
            "<td><input id='updating_phone_employee' type='text' value='"+phone+"'></td>"+
            "<td><input id='updating_mail_employee' type='text' value='"+mail+"'></td>"+
            "<td id='departmentInfo'></td>"+
            "<td><div class='name_table'><button class='accept_update yes_icon' onClick='accept_update_employee("+id+")'></button></div>"+
            "<div class='name_table'><button class='cancel_update no_icon' onClick='cancel_update_employee()'></button></div></td>"+
            "</tr>"

    $("#empTrE"+trId).replaceWith(result);
}

function accept_update_employee(id){
    var nameEmp =  $('#updating_name_employee').val();
        var surnameEmp =  $('#updating_surname_employee').val();
        var patronymicEmp =  $('#updating_patronymic_employee').val();
        var phoneEmp =  $('#updating_phone_employee').val();
        var mailEmp =  $('#updating_mail_employee').val();
        var departmentId = $(".classList").children("option:selected").attr('id');
        var departmentName = $(".classList").children("option:selected").val();
        $.ajax({
                url: '/employee',
                contentType: "application/json",
                type: 'put',
                data: JSON.stringify({employee:{name:nameEmp,
                                                    surname:surnameEmp,
                                                    patronymic: patronymicEmp,
                                                    contacts:{phone:phoneEmp,
                                                              mail:mailEmp
                                                            },
                                                    department:{id:departmentId,
                                                                name:departmentName
                                                            }
                                                    }, id}),

                complete : function() {
                    getAllEmployee();
                },
                success : function(data, textStatus, jqXHR){
                    console.log("dep_update_done")
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("dep_update_error")


                }
            });
}

function cancel_update_employee(){
    getAllEmployee();
}

function createSelectorFromUpdate(values, depId, depName){
    var tables = values;
    var result="<select class='classList'> <option id="+depId+" value=\""+depName+"\">"+depName+"</option>"
    for (i=0; i<tables.length;i++) {
            var departmentId = tables[i].id;
            var departmentName = tables[i].name;
    result+="<option id="+departmentId+" value=\""+departmentName+"\">"+departmentName+"</option>";

    }
    console.log("hello")
    result+="</select>";
    $('#departmentInfo').html(result);
}

function clearSkill(){
    var result="";

    $('#skillsEmployee').html(result);
    $('#skillsEmployee_filter').html(result);
    $('#skillsEmployee_paginate').html(result);
}

function paginationEmp(){
    $('#employee').DataTable( {
            "bJQueryUI":true,
            "bSort":false,
            "bPaginate":true, // Pagination True
            "sPaginationType":"simple_numbers", // And its type.
            "iDisplayLength": 7,
            "searching": true,
            "info": false,
            "bLengthChange": false,
            "destroy": true,
            "language": {
                "search": "Поиск ",
                "paginate": {
                          "previous": "назад",
                          "next": "вперед"
                                    },
                "emptyTable": "Таблица пуста"
              }
        } );
}


function searchEmployeeBySkill(){
     var nameSkill =  $('#searchSkill').val();
     console.log(nameSkill);
    var result = [];
      for (var i = 0; i < nameSkill.length; i++) {
        result.push(nameSkill.charCodeAt(i));
      }

     $.ajax({
     url: '/employee/skills/' + nameSkill,
     contentType: "application/json",
     type: 'get',
     success : function(data) {
                     console.log(data);
                     createTableE(data);
                 },
     error: function (jqXHR, textStatus, errorThrown) {

                 }
     });
}

function delete_empE(id){
    $.ajax({
                url : '/employee/' + id,
                type: 'delete',

                success : function(data) {
                    console.log("del_complite")
                    getAllEmployee();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Таблица навыков у ученика не пуста!")
                }
            });
}


function clearEmpTb(){
    var result="";

    $('#employee').html(result);
    $('#skillsEmployee').html(result);
    $('#employee_filter').html(result);
    $('#employee_paginate').html(result);
    $('#skillsEmployee_filter').html(result);
    $('#skillsEmployee_paginate').html(result);
    $('#searchMainDiv').html(result);
    $('#info').html(result);
    $( "#divInformation" ).removeClass( "divInfo" );
}