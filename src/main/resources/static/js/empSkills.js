$(document).ready(function() {

});

var skillCache= '';

function returnButtonSkill(idEmp){
 var result="<button class='add_skill add_icon' onClick='add_skill("+idEmp+")'></button>";

 $('#addButtonSkills').html(result);
}

function createTableSkillForSearchById(val, idEmp){
    returnButtonSkill(idEmp);
    var table =  val;
    var result="<thead><tr><td><div class='name_table main_cell'>Умения</div><div id='addButtonSkills' class='name_table'></div></td>"+
    "<td>Название</td><td class='descBreak'>Коментарий</td><td>Уровень</td><td>Действия</td></tr></thead><tbody>";

    for (i=0; i<table.length;i++) {
       var id = table[i].id;
       var name = table[i].name;
       var description = table[i].description;
       var level = table[i].level;
       var empId = table[i].empId;
          result+="<tr id='skillTr"+i+"'>"+
          "<td></td>"+
          "<td>"+name+"</td>"+
          "<td class='descBreak'>"+description+"</td>"+
          "<td>"+level+"</td>"+
          "<td><div class='name_table'><button class='del_icon' onClick='delete_skill("+id+", "+idEmp+")' ></button></div>"+
          "<div class='name_table'><button class='update_skill update_icon' onClick='update_skill("+i+","+idEmp+", \""+name+"\", \""+description+"\", "+level+","+id+")' ></button></div></td>"+
          "</tr>"
        }
        result+= "</tbody>"
        $('#skillsEmployee').html(result);
        skillCache = result;
        paginationES();
}

function paginationES(){
    $('#skillsEmployee').DataTable( {
            "bJQueryUI":true,
            "bSort":false,
            "bPaginate":true, // Pagination True
            "sPaginationType":"simple_numbers", // And its type.
            "iDisplayLength": 7,
            "searching": true,
            "info": false,
            "bLengthChange": false,
            "destroy": true,
            "language": {
                "search": "Поиск ",
                "paginate": {
                      "previous": "назад",
                      "next": "вперед"
                    },
                    "emptyTable": "Таблица пуста"
            }
        } );
}

function getSkillsByIdEmp(id){
    var result = '';
    $('#skillsEmployee').html(result);

    $.ajax({
            url : '/skills/employee'+ id,
            type: 'get',
            success : function(data) {
                console.log(data);
                createTableSkillForSearchById(data, id);
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
}

function update_skill(idTr, idEmp, name, description, level, id){
        var result="<tr>"+
        "<td></td>"+
        "<td><input id='updating_skill_0' type='text' value='"+name+"'></td>"+
        "<td><input id='updating_skill_1' type='text' value='"+description+"'></td>"+
        "<td><input id='updating_skill_2' type='text' value='"+level+"'></td>"+
        "<td><div class='name_table'><button class='yes_icon'onClick='accept_updateES("+idEmp+","+id+")'></button></div>"+
        "<div class='name_table'><button class='no_icon' onClick='cancel_updateES("+idEmp+")'></button></div></td>"+
        "</tr>"

        $("#skillTr"+idTr).replaceWith(result);
}

function accept_updateES(idEmp, idSkill){
    var nameSkill =  $('#updating_skill_0').val();
    var descriptionSkill =  $('#updating_skill_1').val();
    var levelSkill =  $('#updating_skill_2').val();

    $.ajax({
            url: '/skills',
            contentType: "application/json",
            type: 'put',
            data: JSON.stringify({skills:{
                                  		id:idSkill,
                                  		name:nameSkill,
                                  		description:descriptionSkill,
                                  		level:levelSkill
                                  	},
                                  	id:idEmp}),

            complete : function() {

                getSkillsByIdEmp(idEmp)
            },
            success : function(data, textStatus, jqXHR){
                console.log("skill_update_done")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("skill_update_error")


            }
        });
}

function cancel_updateES(id){
    getSkillsByIdEmp(id);
}

function add_skill(idEmp){
    var result= skillCache;

        result+="<tr>"+
                "<td></td>"+
                "<td><input id='add_skill_0' type='text' name='name' ></td>"+
                "<td><input id='add_skill_1' type='text' name='name' ></td>"+
                "<td><input id='add_skill_2' type='text' name='name' ></td>"+
                "<td><div class='name_table'><button class='accept_saveD yes_icon' onClick='accept_saveES("+idEmp+")'></button></div>"+
                "<div class='name_table'><button class='cancel_saveD no_icon' onClick='cancel_saveES("+idEmp+")'></button></div></td>"+
                "</tr>";

                $('#skillsEmployee').html(result);
}

function accept_saveES(idEmp){
    var nameSkill = $('#add_skill_0').val();
    var descriptionSkill = $('#add_skill_1').val();
    var levelSkill = $('#add_skill_2').val();

    $.ajax({
                 url : '/skills',
                 contentType: "application/json",
                 type : 'post',
                 data: JSON.stringify({name:nameSkill,
                 description:descriptionSkill,
                 level:levelSkill,
                 employee:{
                 id:idEmp}
                 }),
                 complete : function() {
                     console.log("skill_save_done")
                     getSkillsByIdEmp(idEmp);
                 },

                 error: function (jqXHR, textStatus, errorThrown) {
                    alert("Это хобби уже есть в этой таблице!")
                }
            });
}

function cancel_saveES(idEmp){
    getSkillsByIdEmp(idEmp);
}

function delete_skill(id, idEmp){
            $.ajax({
                url : '/skills/' + id,
                type: 'delete',

                success : function(data) {
                    getSkillsByIdEmp(idEmp);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
}

function returnInfo(){
$("#mainInfo").css({'display':''});

    var result = '';

         $('#department').html(result);
         $('#employeeDepartment').html(result);
         $('#department_paginate').html(result);
         $('#department_filter').html(result);
         $('#employeeDepartment_paginate').html(result);
         $('#employeeDepartment_filter').html(result);
         $(".workBox1").css({ 'width' : '0', 'height' : '0'});
             $('#employee').html(result);
             $('#skillsEmployee').html(result);
             $('#employee_filter').html(result);
             $('#employee_paginate').html(result);
             $('#skillsEmployee_filter').html(result);
             $('#skillsEmployee_paginate').html(result);
             $('#searchMainDiv').html(result);
}
