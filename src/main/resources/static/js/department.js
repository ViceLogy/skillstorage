$(document).ready(function() {
    $("#department_filter").css({ "visibility": "hidden"});
});

var departmentCache = '';

function getAllDepartment(){
    $("#mainInfo").css({'display':'none'});
    $.ajax({
        url : '/department/all',
        type: 'get',
        success : function(data) {
            console.log(data);
            createTableD(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}

function getAllDepartmentFromAddedEmployee(){
    $.ajax({
        url : '/department/all',
        type: 'get',
        success : function(data) {
            console.log(data);
            createSelectorFromAdded(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}

function getAllDepartmentFromUpdateEmployee(depId, depName){
    $.ajax({
            url : '/department/all/'+ depId,
            type: 'get',
            success : function(data) {
                console.log(data);
                createSelectorFromUpdate(data, depId, depName);
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });

}

function createTableD(val){
    var table =  val;
    var result="<thead><tr><td><div class='name_table main_cell'>Классы</div><div class='name_table'><button class='add_dep add_icon' onClick='add_dep()'></button></div></td><td>Действия</td></tr></thead><tbody>";
    for (i=0; i<table.length;i++) {
        var id = table[i].id;
        var name = table[i].name;
        result+="<tr id='depTr"+i+"'>" +
            "<td class = 'get_all_employee_by_dep' onClick='get_all_employee_by_dep("+id+",\""+name+"\"), get_all_employee_by_dep("+id+",\""+name+"\")'>"+ name + "</td>"+
            "<td><div class='name_table'><button class='delete_dep del_icon' onClick='delete_dep("+id+")' ></button></div>"+
            "<div class='name_table'><button class='update_dep update_icon' onClick='update_dep("+id+","+i+",\""+name+"\")' ></button></div></td>"+
            "</tr>";
    }
    result+= "</tbody>"
    $('#department').html(result);
    departmentCache = result;
    pagination();
    }



function pagination(){
    $('#department').DataTable( {
            "bJQueryUI":true,
            "bSort":false,
            "bPaginate":true, // Pagination True
            "sPaginationType":"simple_numbers", // And its type.
            "iDisplayLength": 7,
            "searching": true,
            "info": false,
            "bLengthChange": false,
            "destroy": true,
            "language": {
                "emptyTable": "Таблица пуста"
              }
        } );
}

function delete_dep(id){
    console.log("del_dep CLICK!");
        $.ajax({
            url : '/department/' + id,
            type: 'delete',

            success : function(data) {
                getAllDepartment();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("В классе есть ученики!")
            }
        });
}

function add_dep(){
    var result= departmentCache;

    result+="<tr>"+
            "<td><input id='new_dep' type='text' name='name' style='width:100px;'></td>"+
            "<td><div class='name_table'><button class='accept_saveD yes_icon' onClick='accept_saveD()'></button></div>"+
            "<div class='name_table'><button class='cancel_saveD no_icon' onClick='cancel_saveD()'></button></div></td>"+
            "</tr>";

            $('#department').html(result);
}

function accept_saveD(){
    var nameDep = $('#new_dep').val();
        $.ajax({
             url : '/department',
             contentType: "application/json",
             type : 'post',
             data: JSON.stringify({name:nameDep}),
             complete : function() {
                 console.log("dep_save_done")
                 getAllDepartment();
             },

             error: function (jqXHR, textStatus, errorThrown) {
                alert("Такой класс уже существует")
            }
        });
}

function cancel_saveD(){
    getAllDepartment();
}

function update_dep(id, idTr, name){
   // var result= departmentCache;
    var result="<tr>"+
    "<td><input id='updating_dep' type='text' value='"+name+"'></td>"+
    "<td><div class='name_table'><button class='accept_updateD yes_icon' onClick='accept_updateD("+id+")'></button></div>"+
    "<div class='name_table'><button class='cancel_updateD no_icon' onClick='cancel_updateD()'></button></div></td>"+
    "</tr>"

//    $('#resp_ajax_department').html(result);
    $("#depTr"+idTr).replaceWith(result);
}

function accept_updateD(id){
    var nameDep =  $('#updating_dep').val();
    $.ajax({
        url: '/department',
        contentType: "application/json",
        type: 'put',
        data: JSON.stringify({department:{name:nameDep}, id}),

        complete : function() {

            getAllDepartment();
        },
        success : function(data, textStatus, jqXHR){
            console.log("dep_update_done")
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("dep_update_error")


        }
    });
}

function cancel_updateD(){
    getAllDepartment();
}

function clearDepTb(){
    var result="";

    $('#department').html(result);
    $('#employeeDepartment').html(result);
    $('#department_paginate').html(result);
    $('#department_filter').html(result);
    $('#employeeDepartment_paginate').html(result);
    $('#employeeDepartment_filter').html(result);
    $(".workBox1").css({ 'width' : '0', 'height' : '0'});
    $('#info').html(result);
    $( "#divInformation" ).removeClass( "divInfo" );
}

