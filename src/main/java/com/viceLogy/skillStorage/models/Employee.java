package com.viceLogy.skillStorage.models;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Employee entity
 *
 * @author viceLogy
 * @date 07.04.2019
 */
@Entity
@Table(name = "employee")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    @NotNull
    private String name;

    @Column(name = "surname")
    @NotNull
    private String surname;

    @Column(name = "patronymic")
    @NotNull
    private String patronymic;

    @OneToOne(cascade = CascadeType.ALL)
    private Contacts contacts;

    @ManyToOne
    private Department department;
}