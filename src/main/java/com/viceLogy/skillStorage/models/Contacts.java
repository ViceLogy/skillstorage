package com.viceLogy.skillStorage.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Contacts entity
 *
 * @author viceLogy
 * @date 07.04.2019
 */
@Entity
@Table(name = "contacts")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Contacts implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "phone")
    @Size(min = 11, max = 11)
    private String phone;

    @Column(name = "mail")
    private String mail;


}
