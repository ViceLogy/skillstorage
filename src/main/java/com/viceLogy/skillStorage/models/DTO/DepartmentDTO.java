package com.viceLogy.skillStorage.models.DTO;

import com.viceLogy.skillStorage.models.Department;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data transfer object from departments
 *
 * @author viceLogy
 * @date 24.03.2019
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DepartmentDTO {

    private Long id;
    private Department department;
}
