package com.viceLogy.skillStorage.models.DTO;

import com.viceLogy.skillStorage.models.Skills;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data transfer object from skills
 *
 * @author viceLogy
 * @date 24.03.2019
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SkillDTO {

    private Long id;
    private Skills skills;
}
