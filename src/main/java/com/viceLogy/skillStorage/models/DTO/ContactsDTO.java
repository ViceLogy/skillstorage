package com.viceLogy.skillStorage.models.DTO;

import com.viceLogy.skillStorage.models.Contacts;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data transfer object from contacts
 *
 * @author viceLogy
 * @date 24.03.2019
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContactsDTO {

    private Contacts contacts;
    private Long id;
}
