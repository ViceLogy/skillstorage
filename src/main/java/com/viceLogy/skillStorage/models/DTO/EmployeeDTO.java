package com.viceLogy.skillStorage.models.DTO;

import com.viceLogy.skillStorage.models.Employee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data transfer object from employees
 *
 * @author viceLogy
 * @date 24.03.2019
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDTO {

    private Long id;
    private Employee employee;
}
