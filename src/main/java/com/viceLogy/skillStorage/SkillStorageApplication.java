package com.viceLogy.skillStorage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkillStorageApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkillStorageApplication.class, args);
	}

}
