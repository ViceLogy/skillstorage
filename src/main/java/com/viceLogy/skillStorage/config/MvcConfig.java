package com.viceLogy.skillStorage.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * View configuration
 *
 * @author viceLogy
 * @date 07.04.2019
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    /**
     * return html page login by url /login
     */
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/").setViewName("main");
    }

}