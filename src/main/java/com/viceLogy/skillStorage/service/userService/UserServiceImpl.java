package com.viceLogy.skillStorage.service.userService;

import com.viceLogy.skillStorage.models.User;
import com.viceLogy.skillStorage.repository.UserRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
/**
 * User service
 *
 * @author viceLogy
 * @date 07.04.2019
 */
@Service
@Log
public class UserServiceImpl implements UserService{

    @Autowired
    UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     *Get user by login
     */
    @Override
    public User findUserByLogin(String login) {
        return userRepository.findUserByLogin(login);
    }

    /**
     *Add users
     */
    @Override
    public boolean add(User user){
        try {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            userRepository.save(user);
            return true;
        }catch (Exception ex){
            log.severe(ex.getMessage());
        }
        return false;
    }
}
