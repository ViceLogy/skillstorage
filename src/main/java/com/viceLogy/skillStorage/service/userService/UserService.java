package com.viceLogy.skillStorage.service.userService;

import com.viceLogy.skillStorage.models.User;
/**
 * Interface for creating standard database actions
 *
 * @author viceLogy
 * @date 07.04.2019
 */
public interface UserService {

    User findUserByLogin(String login);
    boolean add(User user);
}
