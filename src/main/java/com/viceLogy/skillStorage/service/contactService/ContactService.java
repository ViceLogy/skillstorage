package com.viceLogy.skillStorage.service.contactService;

import com.viceLogy.skillStorage.models.Contacts;

import java.util.List;
import java.util.Optional;

/**
 * interface for creating standard database actions
 *
 * @author viceLogy
 * @date 24.03.2019
 */
public interface ContactService {

    List<Contacts> getAll();

    Optional<Contacts> getById(Long id);

    boolean add(Contacts contacts);

    boolean update(Contacts contacts, Long id);

    boolean delete(Long id);
}
