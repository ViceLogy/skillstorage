package com.viceLogy.skillStorage.service.contactService;

import com.viceLogy.skillStorage.models.Contacts;
import com.viceLogy.skillStorage.repository.ContactsRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Contact service
 *
 * @author viceLogy
 * @date 24.03.2019
 */
@Log
@Service
public class ContactServiceImpl implements ContactService {

       @Autowired
    ContactsRepository contactsRepo;

    /**
     *Get all contacts
     */
    @Override
    public List<Contacts> getAll() {
        return contactsRepo.findAll();
    }

    /**
     *Get contacts by id
     */
    @Override
    public Optional<Contacts> getById(Long id) {
        return contactsRepo.findById(id);
    }

    /**
     *Save contacts
     */
    @Override
    public boolean add(Contacts contacts) {
        try {
            contactsRepo.save(contacts);
            return true;
        }catch (Exception ex){
            log.severe(ex.getMessage());
        }
        return false;
    }

    /**
     *Update contacts
     */
    @Override
    public boolean update(Contacts contacts, Long id) {
        Contacts contacts1 = null;
        try {
            if(contactsRepo.findById(id).isPresent()) {
                contacts1 = contactsRepo.findById(id).get();
                contacts1.setPhone(contacts.getPhone());
                contacts1.setMail(contacts.getMail());
                contactsRepo.save(contacts1);
                return true;
            }
        }catch (Exception ex){
            log.severe(ex.getMessage());
        }
        return false;
    }

    /**
     *Delete contacts by id
     */
    @Override
    public boolean delete(Long id) {
        try {
            contactsRepo.deleteById(id);
            return true;
        }catch (Exception ex){
            log.severe(ex.getMessage());
        }
        return false;
    }
}
