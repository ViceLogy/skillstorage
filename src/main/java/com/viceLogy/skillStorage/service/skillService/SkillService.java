package com.viceLogy.skillStorage.service.skillService;

import com.viceLogy.skillStorage.models.Skills;
import java.util.List;
import java.util.Optional;

/**
 * Interface for creating standard database actions
 *
 * @author viceLogy
 * @date 07.04.2019
 */
public interface SkillService {

    List<Skills> getAll();
    List<Skills> findByNameOrderByLevelDesc(String name);
    List<Skills> getAllSkillsByIdEmployee(Long empId);
    Skills getSkillByEmpIdAndSkillName(Long empId, String nameSkill);
    Optional<Skills> getById(long id);
    boolean add(Skills skills);
    boolean update(Skills skills, Long id);
    boolean delete(Long id);
    boolean addRandSkill();
}
