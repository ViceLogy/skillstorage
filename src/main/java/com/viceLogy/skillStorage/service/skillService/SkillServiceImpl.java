package com.viceLogy.skillStorage.service.skillService;

import com.viceLogy.skillStorage.models.Employee;
import com.viceLogy.skillStorage.models.Skills;
import com.viceLogy.skillStorage.repository.EmployeeRepository;
import com.viceLogy.skillStorage.repository.SkillsRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Skills service
 *
 * @author viceLogy
 * @date 07.04.2019
 */
@Log
@Service
public class SkillServiceImpl implements SkillService{

    @Autowired
    private SkillsRepository skillsRepo;

    @Autowired
    private EmployeeRepository employeeRepository;

    /**
     *Get all skills
     */
    @Override
    public List<Skills> getAll() {
        return skillsRepo.findAll();
    }

    @Override
    public List<Skills> findByNameOrderByLevelDesc(String name){
        return skillsRepo.findByNameIgnoreCaseContainingOrderByLevelDesc(name);
    }


    /**
     *Get skills by id employee and skill name
     */
    @Override
    public Skills getSkillByEmpIdAndSkillName(Long empId, String nameSkill){
        return skillsRepo.getSkillByEmpIdAndSkillName(empId, nameSkill);
    }

    /**
     *Get all skills by id employee
     */
    public List<Skills> getAllSkillsByIdEmployee(Long empId){
        return skillsRepo.getAllSkillsByIdEmployee(empId);
    }

    /**
     *Get skill by id
     */
    @Override
    public Optional<Skills> getById(long id) {
        return skillsRepo.findById(id);
    }

    /**
     *Save skill
     */
    @Override
    public boolean add(Skills skills) {
        try {
            skillsRepo.save(skills);
            return true;
        }catch (Exception ex){
            log.severe(ex.getMessage());
        }
        return false;
    }

    /**
     *Update skill
     */
    @Override
    public boolean update(Skills skills, Long id) {
        Skills skills1 = null;
        try {
            if(skillsRepo.findById(skills.getId()).isPresent()) {
                skills1 = skillsRepo.findById(skills.getId()).get();
                skills1.setName(skills.getName());
                skills1.setDescription(skills.getDescription());
                skills1.setLevel(skills.getLevel());
                skills1.setEmployee(employeeRepository.getOne(id));
                skillsRepo.save(skills1);
                return true;
            }
        }catch (Exception ex){
            log.severe(ex.getMessage());
        }
        return false;
    }

    /**
     *delete skill by id
     */
    @Override
    public boolean delete(Long id) {
        try {
            skillsRepo.deleteById(id);
            return true;
        }catch (Exception ex){
            log.severe(ex.getMessage());
        }
        return false;
    }

    @Override
    public boolean addRandSkill() {
        try {
            ArrayList<Skills> arrayList = new ArrayList();
            Skills skill0 = new Skills(null, "Программирование", "Знаю Java", 5, null);
            Skills skill1 = new Skills(null, "Кинематография", "Умею работать со звуком и ony Vegas", 5, null);
            Skills skill2 = new Skills(null, "Футбол", "Защитник", 5, null);
            Skills skill3 = new Skills(null, "Музыкант", "Умею петь и играть на гитаре", 5, null);
            arrayList.add(skill0);
            arrayList.add(skill1);
            arrayList.add(skill2);
            arrayList.add(skill3);
            List<Employee> employeeList = employeeRepository.findAll();
            for (Employee employee : employeeList) {
                for (int i = 0; i < 3; i++) {
                    int randIndex = (int) (Math.random() * 4);
                    Skills skills = arrayList.get(randIndex);
                    skills.setEmployee(employee);
                    int randLevel = (int) (Math.random() * 6 + 5);
                    skills.setLevel(randLevel);
                    skillsRepo.save(skills);
                }

            }
            return true;
        }catch (Exception ex){
            log.severe(ex.getMessage());
        }
        return false;
    }

}

