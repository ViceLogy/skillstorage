package com.viceLogy.skillStorage.service.employeeService;

import com.viceLogy.skillStorage.models.Employee;
import com.viceLogy.skillStorage.models.Skills;

import java.util.List;
import java.util.Optional;

/**
 * Interface for creating standard database actions
 *
 * @author viceLogy
 * @date 24.03.2019
 */
public interface EmployeeService {

    List<Employee> getAll();
    List<Employee> getAllByName(String name);
    List<Employee> getAllByDepId(Long id);
    Employee getByNSP(String name, String surname, String patronymic);
    Optional<Employee> getById(Long id);
    boolean add(Employee employee);
    boolean update(Employee employee, Long id);
    boolean delete(Long id);
}
