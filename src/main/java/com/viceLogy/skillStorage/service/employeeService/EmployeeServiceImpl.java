package com.viceLogy.skillStorage.service.employeeService;

import com.viceLogy.skillStorage.models.Employee;
import com.viceLogy.skillStorage.models.Skills;
import com.viceLogy.skillStorage.repository.EmployeeRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Employee service
 *
 * @author viceLogy
 * @date 07.04.2019
 */
@Log
@Service
public class EmployeeServiceImpl implements EmployeeService{

    @Autowired
    private EmployeeRepository employeeRepo;

    /**
     *Get all employees
     */
    @Override
    public List<Employee> getAll() {
        return employeeRepo.findAll();
    }

    /**
     *Get all employee by name
     */
    @Override
    public List<Employee> getAllByName(String name){
        return employeeRepo.getAllByName(name);
    }

    /**
     *Get all employees by dep id
     */
    @Override
    public List<Employee> getAllByDepId(Long id){
        return employeeRepo.findAllByDepId(id);
    }

    /**
     *Get employee by id
     */
    @Override
    public Optional<Employee> getById(Long id) {
       return employeeRepo.findById(id);
    }


    @Override
    public Employee getByNSP(String name, String surname, String patronymic){
        return employeeRepo.getByNSP(name, surname, patronymic);
    }
    /**
     *Save employee
     */
    @Override
    public boolean add(Employee employee) {
        try {
            employeeRepo.save(employee);
            return true;
        }catch (Exception ex){
            log.severe(ex.getMessage());
        }
        return false;
    }

    /**
     *Update employee
     */
    @Override
    public boolean update(Employee employee, Long id) {
        Employee employee1 = null;
        try {
            if(employeeRepo.findById(id).isPresent()) {
                employee1 = employeeRepo.findById(id).get();
                employee1.setName(employee.getName());
                employee1.setSurname(employee.getSurname());
                employee1.setPatronymic(employee.getPatronymic());
                employee1.setContacts(employee.getContacts());
                employee1.setDepartment(employee.getDepartment());
                employeeRepo.save(employee1);
                return true;
            }
        }catch (Exception ex){
            log.severe(ex.getMessage());
        }
        return false;
    }

    /**
     *Delete employee by id
     */
    @Override
    public boolean delete(Long id) {
        try {
            employeeRepo.deleteById(id);
            return true;
        }catch (Exception ex){
            log.severe(ex.getMessage());
        }
        return false;
    }
}
