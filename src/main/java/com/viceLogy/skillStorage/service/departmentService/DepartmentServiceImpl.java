package com.viceLogy.skillStorage.service.departmentService;

import com.viceLogy.skillStorage.models.Department;
import com.viceLogy.skillStorage.repository.DepartmentRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Department service
 *
 * @author viceLogy
 * @date 24.03.2019
 */
@Log
@Service
public class DepartmentServiceImpl implements DepartmentService{

    @Autowired
    private DepartmentRepository departmentRepo;

    /**
     *Get all departments
     */
    @Override
    public List<Department> findAllByOrderByNameDesc() {
        return departmentRepo.findAllByOrderByNameDesc();
    }

    /**
     *Get department by id
     */
    @Override
    public Optional<Department> getById(Long id) {
        return departmentRepo.findById(id);
    }


    @Override
    public Department getDepartmentByName(String name) {
        return departmentRepo.findByName(name);
    }

    /**
     *Save department
     */
    @Override
    public boolean add(Department department) {
        try {
            departmentRepo.save(department);
            return true;
        }catch (Exception ex){
            log.severe(ex.getMessage());
        }
        return false;
    }

    /**
     *Update department
     */
    @Override
    public boolean update(Department department, Long id) {
        Department department1 = null;
        try {
            if(departmentRepo.findById(id).isPresent()) {
                department1 = departmentRepo.findById(id).get();
                department1.setName(department.getName());
                departmentRepo.save(department1);
                return true;
            }
        }catch (Exception ex){
            log.severe(ex.getMessage());
        }
        return false;
    }

    /**
     *Delete department by id
     */
    @Override
    public boolean delete(Long id) {
        try {
            departmentRepo.deleteById(id);
            return true;
        }catch (Exception ex){
            log.severe(ex.getMessage());
        }
        return false;
    }
}
