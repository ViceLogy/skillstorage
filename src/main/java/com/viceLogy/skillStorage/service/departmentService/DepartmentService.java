package com.viceLogy.skillStorage.service.departmentService;

import com.viceLogy.skillStorage.models.Department;
import java.util.List;
import java.util.Optional;

/**
 * Interface for creating standard database actions
 *
 * @author viceLogy
 * @date 24.03.2019
 */
public interface DepartmentService {

    List<Department> findAllByOrderByNameDesc();
    Department getDepartmentByName(String name);
    Optional<Department> getById(Long id);
    boolean add(Department department);
    boolean update(Department department, Long id);
    boolean delete(Long id);
}
