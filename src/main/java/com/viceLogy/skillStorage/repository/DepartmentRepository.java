package com.viceLogy.skillStorage.repository;

import com.viceLogy.skillStorage.models.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * CRUD operations from DepartmentServiceImpl
 *
 * @author viceLogy
 * @date 24.03.2019
 */
@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {

    /**
     * Get by name
     */
    Department findByName(String name);

    /**
     * Get all order by desc
     */
    List<Department> findAllByOrderByNameDesc();
}
