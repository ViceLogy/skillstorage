package com.viceLogy.skillStorage.repository;

import com.viceLogy.skillStorage.models.Contacts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * CRUD operations from ContactServiceImpl
 *
 * @author viceLogy
 * @date 24.03.2019
 */
@Repository
public interface ContactsRepository extends JpaRepository<Contacts, Long> {
}
