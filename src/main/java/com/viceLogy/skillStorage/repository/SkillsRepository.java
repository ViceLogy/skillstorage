package com.viceLogy.skillStorage.repository;

import com.viceLogy.skillStorage.models.Skills;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * CRUD operations from SkillServiceImpl
 *
 * @author viceLogy
 * @date 24.03.2019
 */
@Repository
public interface SkillsRepository extends JpaRepository<Skills, Long> {
    /**
     * Get all skills by id employee
     */
    @Query(value = "SELECT * FROM skills WHERE employee_id = :empId", nativeQuery = true)
    List<Skills> getAllSkillsByIdEmployee(@Param("empId") Long empId);

    /**
     * Get skill by id employee and skill name
     */
    @Query(value = "select name, employee_id FROM skills WHERE name=:skillName AND employee_id = :empId", nativeQuery = true)
    Skills getSkillByEmpIdAndSkillName(@Param("empId") Long empId, @Param("skillName") String skillName);

    List<Skills> findByNameIgnoreCaseContainingOrderByLevelDesc(String name);
}
