package com.viceLogy.skillStorage.repository;

import com.viceLogy.skillStorage.models.Employee;
import com.viceLogy.skillStorage.models.Skills;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * CRUD operations from EmployeeServiceImpl
 *
 * @author viceLogy
 * @date 07.04.2019
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    /**
     * Get all employee by department id
     */
    @Query( value = "SELECT * FROM employee WHERE department_id = :id", nativeQuery = true)
    List<Employee> findAllByDepId(@Param("id") Long id);

    /**
     * Get all employee by name
     */
    @Query(value = "SELECT * FROM employee WHERE id IN (SELECT employee_id FROM skills WHERE name = :name ORDER BY level)", nativeQuery = true)
    List<Employee> getAllByName(@Param("name") String name);

    /**
     * Get by name, surname, patronymic
     */
    @Query(value = "SELECT name, surname, patronymic FROM employee WHERE name = :name and surname = :surname and patronymic = :patronymic", nativeQuery = true)
    Employee getByNSP(@Param("name") String name, @Param("surname") String surname, @Param("patronymic") String patronymic);
}
