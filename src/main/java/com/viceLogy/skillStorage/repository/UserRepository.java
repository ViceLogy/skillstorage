package com.viceLogy.skillStorage.repository;

import com.viceLogy.skillStorage.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * CRUD operations from UserServiceImpl
 *
 * @author viceLogy
 * @date 07.04.2019
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    /**
     * Get skill by login
     */
    User findUserByLogin(String login);
}
