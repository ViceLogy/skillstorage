package com.viceLogy.skillStorage.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
/**
 * Controllers from return html page
 *
 * @author viceLogy
 * @date 07.04.19
 */
@Controller
public class MainViewController {
    /**
     * Return main page
     */
    @GetMapping("/main")
    public String mainPage(){
        return "main";
    }

    /**
     * Return registration page
     */
    @GetMapping("/registration")
    public String registration(){
        return "registration";
    }
}
