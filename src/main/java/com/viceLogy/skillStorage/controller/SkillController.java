package com.viceLogy.skillStorage.controller;

import com.viceLogy.skillStorage.models.DTO.SkillDTO;
import com.viceLogy.skillStorage.models.Skills;
import com.viceLogy.skillStorage.service.skillService.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controllers from skills
 *
 * @author viceLogy
 * @date 07.04.2019
 */
@RestController
@RequestMapping("/skills")
public class SkillController {

    @Autowired
    SkillService skillService;

    /**
     * Get all skills
     */
    @GetMapping
    public ResponseEntity getAllSkills(){
        List<Skills> skillsList = skillService.getAll();
        if (skillsList != null){
            return new ResponseEntity<>(skillsList, HttpStatus.OK);
        }else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get skills by id employee
     */
    @GetMapping("/employee{empId}")
    public ResponseEntity getAllSkillsByIdEmployee(@PathVariable Long empId){
        List<Skills> skillsList = skillService.getAllSkillsByIdEmployee(empId);
        if (skillsList != null){
            return new ResponseEntity<>(skillsList, HttpStatus.OK);
        }else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get skill by id
     */
    @GetMapping("/{id}")
    public ResponseEntity getSkillById(@PathVariable Long id){
        if (id != null){
            skillService.getById(id);
            return new ResponseEntity(HttpStatus.OK);
        }else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Save skills
     */
    @PostMapping
    public ResponseEntity saveSkills(@RequestBody Skills skills){
        if (skills != null){
            if (skillService.getSkillByEmpIdAndSkillName(skills.getEmployee().getId(), skills.getName())== null) {
                skillService.add(skills);
                return new ResponseEntity(HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        }else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * update skills
     */
    @PutMapping
    public ResponseEntity updateSkills(@RequestBody SkillDTO skills){
        if (skills != null){
            skillService.update(skills.getSkills(), skills.getId());
            return new ResponseEntity(HttpStatus.OK);
        }else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * delete skills
     */
    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable Long id){
        if (id != null) {
            skillService.delete(id);
            return new ResponseEntity(HttpStatus.OK);
        }else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/utility/randSkills")
    public ResponseEntity utilitySaveSkills(){
        skillService.addRandSkill();
        return new ResponseEntity(HttpStatus.OK);
    }
}
