package com.viceLogy.skillStorage.controller;

import com.viceLogy.skillStorage.models.Role;
import com.viceLogy.skillStorage.models.User;
import com.viceLogy.skillStorage.service.userService.UserService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Collections;
import java.util.Map;

/**
 * Controllers from users
 *
 * @author viceLogy
 * @date 07.04.19
 */
@Log
@Controller
public class UserController {

    @Autowired
    UserService userService;

    /**
     * Add new user of registration form
     */
    @PostMapping("/registration")
    public String addUser (User user, Map<String, Object> model){
        log.severe(user.toString());
        User userFromDb = userService.findUserByLogin(user.getLogin());

        if (userFromDb != null){
            model.put("message", "Пользователь с этим адресом уже существует");
            return "registration";
        } else {
            user.setActive(true);
            user.setRoles(Collections.singleton(Role.USER));
            userService.add(user);
            return "redirect:/login";
        }
    }

    @GetMapping("/loginError")
    public String loginError(Map<String, Object> model){
        model.put("errorMassage", "Неверно введён логин или пароль");
        return "login";
    }

}
