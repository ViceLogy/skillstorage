package com.viceLogy.skillStorage.controller;

import com.viceLogy.skillStorage.models.DTO.DepartmentDTO;
import com.viceLogy.skillStorage.models.Department;
import com.viceLogy.skillStorage.service.departmentService.DepartmentService;
import com.viceLogy.skillStorage.service.employeeService.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controllers from departments
 *
 * @author viceLogy
 * @date 07.04.2019
 */
@RestController
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private EmployeeService employeeService;

    /**
     * Get all department
     */
    @GetMapping("/all")
    public ResponseEntity getAllDepartment() {
        List<Department> departmentList = departmentService.findAllByOrderByNameDesc();
        if (departmentList != null) {
            return new ResponseEntity<>(departmentList, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
    /**
     * Get all department from employee table
     */
    @GetMapping("/all/{id}")
    public ResponseEntity getAllDepartmentFromEmployeeTable(@PathVariable Long id) {
        List<Department> departmentList = departmentService.findAllByOrderByNameDesc();
        if (departmentList != null) {
            for (Department department : departmentList) {
                if (department.getId() == id) {
                    departmentList.remove(department);
                    return new ResponseEntity<>(departmentList ,HttpStatus.OK);
                }
            }
            return new ResponseEntity<>(departmentList ,HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get department by id
     */
    @GetMapping("/{id}")
    public ResponseEntity getDepartmentById(@PathVariable Long id){
        if (id != null){
            departmentService.getById(id);
            return new ResponseEntity(HttpStatus.OK);
        }else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Save department
     */
    @PostMapping
    public ResponseEntity saveDepartment(@RequestBody Department department) {
        if (department != null) {
            if(departmentService.getDepartmentByName(department.getName()) == null){
                departmentService.add(department);
                return new ResponseEntity(HttpStatus.OK);
            }else {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Update department
     */
    @PutMapping
    public ResponseEntity updateDepartment(@RequestBody DepartmentDTO department) {
        if (department != null) {
            departmentService.update(department.getDepartment(), department.getId());
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Delete department
     */
    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable Long id) {
        if (employeeService.getAllByDepId(id).isEmpty()) {
            departmentService.delete(id);
            return new ResponseEntity(HttpStatus.OK);
        }else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
