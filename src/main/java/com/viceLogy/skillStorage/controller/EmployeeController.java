package com.viceLogy.skillStorage.controller;

import com.viceLogy.skillStorage.models.DTO.EmployeeDTO;
import com.viceLogy.skillStorage.models.Employee;
import com.viceLogy.skillStorage.models.Skills;
import com.viceLogy.skillStorage.service.employeeService.EmployeeService;
import com.viceLogy.skillStorage.service.skillService.SkillService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controllers from employees
 *
 * @author viceLogy
 * @date 07.04.19
 */
@Log
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private SkillService skillService;

    /**
     * Get all employee
     */
    @GetMapping("/all")
    public ResponseEntity getAllEmployee() {
        List<Employee> employeeList = employeeService.getAll();
        if (employeeList != null) {
            return new ResponseEntity<>(employeeList, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get all skill by name
     */
    @GetMapping("/skills/{name}")
    public ResponseEntity getAllByName(@PathVariable String name) {
        log.severe(name);
        if (name != null){
            List<Employee> listEmployee = new ArrayList<>();
            List<Skills> skillList = skillService.findByNameOrderByLevelDesc(name);
            if (skillList != null) {
                for (Skills skill : skillList) {
                    listEmployee.add(skill.getEmployee());
                }
            }
            log.severe(skillList.toString());
            if (!listEmployee.isEmpty()){
                return new ResponseEntity<>(listEmployee, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
            }
        } else {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
        }

    }

    /**
     * Get all employee by id department
     */
    @GetMapping("/dep{id}")
    public ResponseEntity getAllEmployeeByDepId(@PathVariable Long id){
        List<Employee> employeeList = employeeService.getAllByDepId(id);
        if (employeeList != null){
            return new ResponseEntity(employeeList ,HttpStatus.OK);
        }else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get employee by id
     */
    @GetMapping("/{id}")
    public ResponseEntity getEmployeeById(@PathVariable Long id){
        if (id != null){
            employeeService.getById(id);
            return new ResponseEntity(HttpStatus.OK);
        }else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Save employee
     */
    @PostMapping
    public ResponseEntity saveEmployee(@RequestBody Employee employee) {
        if (employee != null) {
            if (employeeService.getByNSP(employee.getName(), employee.getSurname(), employee.getPatronymic()) == null) {
                employeeService.add(employee);
                return new ResponseEntity(HttpStatus.OK);
            } else {
                return new  ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Update employee
     */
    @PutMapping
    public ResponseEntity updateEmployee(@RequestBody EmployeeDTO employee) {
        if (employee != null) {
            employeeService.update(employee.getEmployee(), employee.getId());
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Delete department
     */
    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable Long id) {
        if (skillService.getAllSkillsByIdEmployee(id).isEmpty()) {
            employeeService.delete(id);
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
