package com.viceLogy.skillStorage.controller;

import com.viceLogy.skillStorage.models.Contacts;
import com.viceLogy.skillStorage.models.DTO.ContactsDTO;
import com.viceLogy.skillStorage.service.contactService.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controllers from contacts
 *
 * @author viceLogy
 * @date 07.04.2019
 */
@RestController
@RequestMapping("/contacts")
public class ContactController {

    @Autowired
    private ContactService contactService;

    /**
     * Get all contacts
     */
    @GetMapping("/all")
    public ResponseEntity getAllContacts(){
        List<Contacts> contactsList = contactService.getAll();
        if (contactsList != null){
            return new ResponseEntity<>(contactsList, HttpStatus.OK);
        }else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get contacts by id
     */
    @GetMapping("/get/{id}")
    public ResponseEntity getContactsById(@PathVariable Long id){
        if (id != null){
            contactService.getById(id);
            return new ResponseEntity(HttpStatus.OK);
        }else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Save contacts
     */
    @PostMapping("/save")
    public ResponseEntity saveContact(@RequestBody Contacts contacts){
        if (contacts != null){
            contactService.add(contacts);
            return new ResponseEntity(HttpStatus.OK);
        }else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Update contacts
     */
    @PutMapping("/update")
    public ResponseEntity updateContact(@RequestBody ContactsDTO contacts){
        if (contacts != null){
            contactService.update(contacts.getContacts(), contacts.getId());
            return new ResponseEntity(HttpStatus.OK);
        }else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Delete contacts by id
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteById(@PathVariable Long id){
        if (id != null) {
            contactService.delete(id);
            return new ResponseEntity(HttpStatus.OK);
        }else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
